const getItems = () =>
	new Promise(resolve => {
		const items = [...document.querySelectorAll('[data-test-pin-id]')]
			.filter($el => $el.getAttribute('data-test-pin-id').length > 32)
			.map($el => $el.closest('[data-grid-item="true"]'))
			.filter($el => $el);

		resolve(items);
	});

const remover = () => {
	getItems().then(items => {
		blockedAdd(items.length);

		items.forEach($el => {
			[...$el.children].forEach($el2 => $el2.remove());

			const $gradient = document.createElement("div");

			$gradient.classList.add(
				"pinteblock-gradient",
				randomGradient()
			);

			$el.appendChild($gradient);
		});
	});
};

(function () {
	console.debug("On a Pinterest Domain!");

	window.addEventListener("scroll", debounce(remover));

	remover();
})();
